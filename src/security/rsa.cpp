#include "security/rsa.hpp"

Extended_Euclidean_Algorithm_container eea(long a, long b) {
	Extended_Euclidean_Algorithm_container eea_container;
	if (b == 0) {
		eea_container.var1 = 1;
		eea_container.var2 = 0;
		return eea_container;
	}
	Extended_Euclidean_Algorithm_container alpha, beta;
	alpha.var1 = a / b;
	alpha.var2 = a % b;
	beta = eea(b, alpha.var2);

	eea_container.var1 = beta.var2;
	eea_container.var2 = beta.var1 - (alpha.var1 * beta.var2);
	return eea_container;
}

long find_inverse(long x, long y) {
	long inv = eea(x, y).var1;
	if (inv < 1)
		inv += y;
	return inv;
}

RSA_key_container::RSA_key_container() {
	valid = false;
}

RSA_key_container::RSA_key_container(long p, long q) {
	long E = randomPrime((p - 1) * (q - 1) - 1);
	//cout << "The generated value of E is : " << E << endl;
	valid = true;
	prime_p = p;
	prime_q = q;
	modulus = p * q;
	long T = (prime_p - 1) * (prime_q - 1);
	if (!isPrime(prime_p) || !isPrime(prime_q)) {
		//cout << "Something wrong with the primality!" << endl;
		valid = false;
	} else if ((E < 1) || (E > T)) {
		//cout << "Something wrong with the RSA conditions!" << endl;
		valid = false;
	} else if ((T % E == 0)) {
		//cout << "Something wrong with the other RSA conditions!" << endl;
		valid = false;
	} else {
		public_key = E;
		private_key = find_inverse(E, T);
	}
}

bool RSA_key_container::get_validity() {
	return valid;
}

void RSA_key_container::set_validity(bool validity) {
	valid = validity;
}

long RSA_key_container::get_private_key() {
	return private_key;
}

void RSA_key_container::set_private_key(long new_private_key) {
	private_key = new_private_key;
}

long RSA_key_container::get_public_key() {
	return public_key;
}

void RSA_key_container::set_public_key(long new_public_key) {
	public_key = new_public_key;
}

long RSA_key_container::get_modulus() {
	return modulus;
}

void RSA_key_container::set_modulus(long new_modulus) {
	modulus = new_modulus;
}

long RSA_key_container::encrypt_RSA(long plaintext) {
	return crtModulus(plaintext, get_public_key(), get_modulus());
}

long RSA_key_container::decrypt_RSA(long ciphertext) {
	return crtModulus(ciphertext, get_private_key(), get_modulus());
}
