#include <stdio.h>
#include <time.h>

long custom_rand(long lim);

long crtModulus(long base, long exponent, long modulus);

int isPrime(long num);

long nextPrime(long num);

long randomPrime(long upper_limit);
